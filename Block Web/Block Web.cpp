﻿// Block Web.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
#include "resource1.h"
#include <Winuser.h>
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <fstream>
#include<string>
#include<vector>
using namespace std;

#define EXPORT  __declspec(dllexport)
#define MAX_CHAR 128

HHOOK hHookKeyboard = NULL;
HINSTANCE hinstLib;
char str[MAX_CHAR], web[MAX_CHAR], Data[MAX_CHAR];
int lock = 0, block = 0;

//Copy file host vao C:\Window\...\etc de chan trang web
bool CopyHostFile(string src, string dest, int type)
{
	ifstream fin(src);
	ofstream fout;
	string s;

	if (fin.is_open())
	{
		if (type == 0)
		{
			fout.open(dest);
			while (!fin.eof())
			{
				getline(fin, s);

				if (s != "")
					fout << s << endl;
			}
		}
		else
		{
			fout.open(dest, ios::app);

			while (!fin.eof())
			{
				getline(fin, s);
				if (s != "")
					fout <<"127.0.0.1\t"<< s << endl;
			}
		}

		fout.close();
		fin.close();
		return true;
	}
	return false;
}

//Cap nhat list box tu file khi ung dung khoi tao
EXPORT void _UpdateListBox(HWND hWnd, int ID)
{
	ifstream fin("web");
	string s;
	char *buf;

	if (fin.is_open())
	{
		while (!fin.eof())
		{
			getline(fin, s);
			
			if (s.compare("") != 0)
			{
				buf = new char[s.length() + 1];
				copy(s.begin(), s.end(), buf);
				buf[s.length()] = '\0';
				SendDlgItemMessage(hWnd, ID, LB_ADDSTRING, 0, (LPARAM)buf);
			}
		}
		fin.close();
	}
}

//Cap nhat danh sach trang web bi chan trong file tu list box
// Ham nay duoc goi khi user nhan Huy Chan, Them, Huy tat ca
EXPORT void _UpdateWebFile(HWND hWnd, int ID)
{
	int t = 0, len;
	LRESULT a;
	char *buf;
	ofstream fout("web");

	//Lay do dai cua chuoi
	len = (int)SendDlgItemMessage(hWnd, ID, LB_GETTEXTLEN, 0, (LPARAM)t);

	while (len != 0)
	{
		//Lay chuoi tu list box
		buf = new char[len + 1];
		a = SendDlgItemMessage(hWnd, ID, LB_GETTEXT, (WPARAM)t, (LPARAM)buf);
		if (a == -1)
			break;

		//Chuyen char-> string
		fout << buf << endl;
		if (strstr(buf, "www.") == NULL)
			fout <<"www." <<buf << endl;
		/*if (strstr(buf, "http://") == NULL)
			fout << "http://" << buf << endl;
		if (strstr(buf, "https://") == NULL)
			fout << "https://" << buf << endl;
		if (strstr(buf, "http://www.") == NULL)
			fout << "http://www." << buf << endl;
		if (strstr(buf, "https://www.") == NULL)
			fout << "https://www." << buf << endl;*/
		
		//Lay do dai cua chuoi
		t++;
		len = (int)SendDlgItemMessage(hWnd, ID, LB_GETTEXTLEN, 0, (LPARAM)t);
	}

	//Gan chuoi str = NULL
	memset(str, 0, sizeof str);
	//Gan chuoi data = NULL
	memset(Data, 0, sizeof Data);

	if (CopyHostFile("root", "hosts", 0))
	{
		if (CopyHostFile("web", "hosts", 1))
			CopyHostFile("hosts","C:\\Windows\\System32\\drivers\\etc\\hosts", 0);
	}
	fout.close();
}

//Kiem tra xem mo duoc file host o C:\Window\...\etc ko
EXPORT bool _checkFileHost()
{
	ofstream fin;
	fin.open("C:\\Windows\\System32\\drivers\\etc\\hosts");

	if (fin.is_open())
	{
		fin.close();
		return true;
	}

	return false;
}

//Kiem tra xem key word nguoi dung nhap co phu hop voi trang
//web bi chan trong danh sach khong
bool doCheckKeyWord(char str[])
{
	ifstream fin("web");
	string s;

	//Neu chuoi nhap vao = NULL => return
	if (str == NULL)
		return false;

	if (fin.is_open())
	{
		while (!fin.eof())
		{
			getline(fin, s);
			if (s.length() == 0) //Neu chuoi lay tu file = 0 =>return
				return false;


			if (s.length() >= strlen(str)) //Neu chuoi lay tu file > chuoi so sanh
			{

				if (strstr(s.c_str(), str) != NULL)
				{

					strcpy(web, s.c_str());
					return true;
				}
			}
			else //Neu chuoi lay tu file < chuoi so sanh
			{
				if (strstr(str, s.c_str()) != NULL)
				{
					strcpy(web, s.c_str());
					return true;
				}
			}
		}
		fin.close();
	}
	return false;
}

//Ghi du lieu user nhap xuong file
void writeDataToFile(char data[])
{
	ofstream fout("data.txt", ios::app);

	if (!fout.is_open())
		fout.open("data.txt");
	
	_strlwr(data);
	fout << data << endl;
	fout.close();
}

//Xoa ten trang web trong file web
void doDeleteWebName(char web[])
{
	ifstream fin("web");
	vector<string> s;
	string st;

	//Neu chuoi nhap vao = NULL => return
	if (web == NULL)
		return;

	if (fin.is_open())
	{
		while (!fin.eof())
		{
			getline(fin, st);

			//So sanh 2 trang web trong file va trang web phat hien duoc
			if (st.length() >= strlen(web)) //Neu chuoi lay tu file > chuoi so sanh
			{
				if (strstr(st.c_str(), web) == NULL)
					s.push_back(st);
			}
			else //Neu chuoi lay tu file < chuoi so sanh
			{
				if (strstr(web, st.c_str()) == NULL)
					s.push_back(st);
			}
		}
		fin.close();
	}

	ofstream fout("web");
	int t = s.size(), i = 0;

	while (i < t)
	{
		fout << s[i] << endl;
		i++;
	}

	if (CopyHostFile("root", "hosts", 0))
	{
		if (CopyHostFile("web", "hosts", 1))
			CopyHostFile("hosts", "C:\\Windows\\System32\\drivers\\etc\\hosts", 0);
	}
	memset(web, 0, sizeof web);
	fout.close();
}

//Dialog warming proc
INT_PTR CALLBACK WarmingProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	char temp[1000], buf[120];
	int len;
	static char stri[MAX_CHAR];
	HFONT hFont;

	UNREFERENCED_PARAMETER(lParam);
	
	switch (message)
	{
	case WM_INITDIALOG:
		strcpy(stri, str);

		//Set font
		hFont = CreateFont(0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, TEXT("Courier New"));
		SendDlgItemMessage(hDlg, IDC_EDIT, WM_SETFONT, (WPARAM)hFont, 0);
		SendDlgItemMessage(hDlg, IDC_TEXT, WM_SETFONT, (WPARAM)hFont, 0);

		strcpy(temp, web);
		strcat(temp, "\nTrang wed nay dang bi chan!");
		strcat(temp, "\nNeu ban muon truy cap lai trang wed nay.");
		strcat(temp, "\nVui long nhap mat khau.");
		SetDlgItemText(hDlg, IDC_TEXT, temp);

		return (INT_PTR)TRUE;
	case WM_COMMAND:
		switch (wParam)
		{
		case IDOK:
			len = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT));
			if (len > 0)
			{
				GetDlgItemText(hDlg, IDC_EDIT, buf, len + 1);
				buf[len] = 0;
			}

			if (strcmp(buf, "123456789") == 0)
			{
				strcpy(temp, web);
				strcat(temp, "\nTrang wed nay da duoc mo.");
				strcat(temp, "\nVui long khoi dong lai trang wed");
				SetDlgItemText(hDlg, IDC_TEXT, temp);
				doDeleteWebName(stri);
				EndDialog(hDlg, LOWORD(wParam));
				
			}
			else if (strcmp(buf, "123456789") != 0)
				SetDlgItemText(hDlg, IDC_TEXT, "Mat khau cua ban nhap chua dung! \n Vui long nhap lai");
			break;
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		default:
			return (INT_PTR)FALSE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//Tao dialog canh bao de nguoi dung nhap mat khau
void doCreateDialogWarming(HINSTANCE hInst)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_WARM), 0, WarmingProc);
}

//Keyboard hook
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	int t = 0;
	char c[128];
	if (nCode < 0)
		return CallNextHookEx(hHookKeyboard, nCode, wParam, lParam);

	KBDLLHOOKSTRUCT *kv = (KBDLLHOOKSTRUCT*)lParam;
	
	//Neu phim duoc nhan != Enter va Phim do nhan xuong 
	if (kv->vkCode != VK_RETURN && wParam == WM_KEYDOWN)
	{
		if (kv->vkCode != VK_BACK)
		{			
			sprintf(c, "%c", LOBYTE(kv->vkCode));
			if (str == NULL)
				strcpy(str, c);
			else if (strlen(str) <= 10) //Chi nhan toi da 10 ki tu
				strcat(str, c);			//Vi neu nhan qua 10 ki tu se gay kho khan cho viec so sanh

			if (Data == NULL)
				strcpy(Data, c);
			else
				strcat(Data, c);
		}
		else //Neu user nhan backspace thi xoa di 1 ki tu
		{
			if (strlen(str) > 1)
				str[strlen(str) - 1] = 0;
			if (strlen(Data) > 1)
				Data[strlen(Data) - 1] = 0;
		}
	}
	//Nguoc lai kiem tra them do dai cua chuoi > 1 ko?
	else if (kv->vkCode == VK_RETURN /*&& strlen(str) > 1*/ && wParam == WM_KEYDOWN)
	{
		_strlwr(str);
		//Neu key word co nam trong danh sach chan, thi thong bao = Messagebox
		if (doCheckKeyWord(str) == true /*&& strlen(str) >=2*/) 
		{
			doCreateDialogWarming(hinstLib);
			memset(web, 0, sizeof web); //gan chuoi web = NULL
		}
		
		//Ghi du lieu user nhap xuong file
		writeDataToFile(Data);

		memset(str, 0, sizeof str); //gan chuoi str = NULL
		memset(Data, 0, sizeof Data); //gan chuoi data = NULL
		return CallNextHookEx(hHookKeyboard, nCode, wParam, lParam);
	}

	return CallNextHookEx(hHookKeyboard, nCode, wParam, lParam);
}

EXPORT void _doInstallHook(HWND hWnd)
{
	//Install keyboard hook
	if (hHookKeyboard == NULL)
		hHookKeyboard = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, hinstLib, 0);

	//Update Host File
	CopyHostFile("hosts", "C:\\Windows\\System32\\drivers\\etc\\hosts", 0);

	//tao file data de lu du lieu
	ofstream fout("data.txt");
	fout.close();

	memset(str, 0, sizeof str); //gan chuoi str = NULL
	memset(Data, 0, sizeof Data); //gan chuoi data = NULL
}

EXPORT void _doRemoveHook(HWND hWnd)
{
	if (hHookKeyboard != NULL)
	{
		UnhookWindowsHookEx(hHookKeyboard);
		hHookKeyboard = NULL;
	}
}