//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by G9.rc
//
#define IDC_MYICON                      2
#define ID_HUY                          3
#define IDD_G9_DIALOG                   102
#define IDD_G9                          102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_G9                          107
#define IDI_SMALL                       108
#define IDC_G9                          109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDC_LIST1                       1000
#define IDC_EDIT1                       1001
#define ID_HUY_ALL                      1002
#define IDC_TRAY                        1003
#define IDC_BUTTON1                     1004
#define IDC_EXIT                        1004
#define IDC_TEXT                        1005
#define IDC_EDIT2                       1006
#define ID_TRAY_APP_ICON                5000
#define IDC_STATIC                      -1
#define WM_TRAYICON	(WM_USER + 1)

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
