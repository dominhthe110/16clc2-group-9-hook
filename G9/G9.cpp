// G9.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "G9.h"
#include <Shellapi.h>
#include <Strsafe.h>
#include <Commctrl.h>

#define MAX_LOADSTRING 100

// Global Variables:
HWND hWnd;
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
int pass = 0;
bool check;

// Forward declarations of functions included in this code module:
BOOL				InitInstance(HINSTANCE, int);
void doInstallHook(HWND hWnd);
void doRemoveHook(HWND hWnd);
bool checkFileHost();
INT_PTR CALLBACK HookBloking(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void UpdateListBox(HWND hWnd, int ID);
void UpdateWebFile(HWND hWnd, int ID);
void showInSystemTray(HWND hWnd);
INT_PTR CALLBACK WarmingProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;



	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!IsDialogMessage(hWnd, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}


BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_G9), NULL, HookBloking);
	ShowWindow(hWnd, SW_SHOW);

	return TRUE;
}

INT_PTR CALLBACK HookBloking(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	HFONT hFont;
	wchar_t buf[60];
	int  len;

	switch (message)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hWnd, IDC_EDIT1));

		//Set font
		hFont = CreateFont(0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, TEXT("Courier New"));
		SendDlgItemMessage(hWnd, IDC_EDIT1, WM_SETFONT, (WPARAM)hFont, 0);
		SendDlgItemMessage(hWnd, IDC_LIST1, WM_SETFONT, (WPARAM)hFont, 0);

		//Set enable button 
		EnableWindow(GetDlgItem(hWnd, IDOK), FALSE);
		EnableWindow(GetDlgItem(hWnd, ID_HUY), FALSE);
		EnableWindow(GetDlgItem(hWnd, ID_HUY_ALL), FALSE);

		UpdateListBox(hWnd, IDC_LIST1);

		//Kiem tra xem mo duoc file host ko? Neu ko mo duoc thi tat ung dung
		if (checkFileHost() == false)
		{
			MessageBox(hWnd, L"Vui long khoi dong ung dung voi quyen ADMIN\nClick chuot phai vao ung dung\nChon Run as Administrator",
				L"Thong bao", MB_OK | MB_ICONINFORMATION);

			EndDialog(hWnd, LOWORD(wParam));
			PostQuitMessage(0);
			return (INT_PTR)TRUE;
		}

		doInstallHook(hWnd);
		return (INT_PTR)FALSE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_EDIT1:
			SendMessage(hWnd, DM_SETDEFID, (WPARAM)IDOK, 0); //Set default button
			EnableWindow(GetDlgItem(hWnd, IDOK), TRUE);

			break;
		case IDOK:
			int nTimes, index;

			nTimes = 1;
			len = GetWindowTextLength(GetDlgItem(hWnd, IDC_EDIT1));

			if (len > 0)
			{
				//Allocate and get the string into our buffer
				GetDlgItemText(hWnd, IDC_EDIT1, buf, len + 1);
				buf[len] = 0;

				//Add the string to the list box
				index = SendDlgItemMessage(hWnd, IDC_LIST1, LB_ADDSTRING, 0, (LPARAM)buf);
				SendDlgItemMessage(hWnd, IDC_LIST1, LB_SETITEMDATA, (WPARAM)index, (LPARAM)nTimes);

				SetDlgItemText(hWnd, IDC_EDIT1, 0);
				SetFocus(GetDlgItem(hWnd, IDC_EDIT1));
				EnableWindow(GetDlgItem(hWnd, IDOK), FALSE);
				UpdateWebFile(hWnd, IDC_LIST1);

				MessageBox(hWnd, L"Chan thanh cong\nVui long khoi dong lai trinh duyet de ap dung!", L"Thong bao", MB_OK | MB_ICONINFORMATION);
			}
			break;

		case IDC_LIST1:
			if (SendDlgItemMessage(hWnd, IDC_LIST1, LB_GETCURSEL, 0, 0) != LB_ERR)
			{
				SendMessage(hWnd, DM_SETDEFID, (WPARAM)ID_HUY, 0); //Set default button

				EnableWindow(GetDlgItem(hWnd, ID_HUY), TRUE);
				EnableWindow(GetDlgItem(hWnd, ID_HUY_ALL), TRUE);
				EnableWindow(GetDlgItem(hWnd, IDOK), FALSE);
			}
			break;
		case ID_HUY:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), 0, WarmingProc);
			if (check == true)
			{
				LRESULT id;
				HWND hList;

				hList = GetDlgItem(hWnd, IDC_LIST1);
				id = SendMessage(hList, LB_GETCURSEL, 0, 0);

				if (id != -1)
				{
					SendMessage(hList, LB_DELETESTRING, id, 0);

					SetFocus(GetDlgItem(hWnd, IDC_LIST1));
					EnableWindow(GetDlgItem(hWnd, ID_HUY), FALSE);
					UpdateWebFile(hWnd, IDC_LIST1);

					MessageBox(hWnd, L"Huy thanh cong\nVui long khoi dong lai trinh duyet de ap dung!", L"Thong bao", MB_OK | MB_ICONINFORMATION);
				}
				id = SendMessage(hList, LB_GETCOUNT, 0, 0);
				if (id == 0)
				{
					EnableWindow(GetDlgItem(hWnd, IDOK), TRUE);
					SetFocus(GetDlgItem(hWnd, IDC_EDIT1));
					EnableWindow(GetDlgItem(hWnd, ID_HUY_ALL), FALSE);
					EnableWindow(GetDlgItem(hWnd, ID_HUY), FALSE);
				}
			}
			break;
		case ID_HUY_ALL:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), 0, WarmingProc);
			if (check == true)
			{
				SendDlgItemMessage(hWnd, IDC_LIST1, LB_RESETCONTENT, 0, 0);

				SendMessage(hWnd, DM_SETDEFID, (WPARAM)IDOK, 0); //Set default button
				SetFocus(GetDlgItem(hWnd, IDC_EDIT1));

				EnableWindow(GetDlgItem(hWnd, ID_HUY), FALSE);
				EnableWindow(GetDlgItem(hWnd, ID_HUY_ALL), FALSE);
				UpdateWebFile(hWnd, IDC_LIST1);

				MessageBox(hWnd, L"Huy thanh cong\nVui long khoi dong lai trinh duyet de ap dung!", L"Thong bao", MB_OK | MB_ICONINFORMATION);
			}
			break;
		case IDC_TRAY:
			showInSystemTray(hWnd);
			ShowWindow(hWnd, SW_HIDE);
			break;
		case IDC_EXIT:
			doRemoveHook(hWnd);
			EndDialog(hWnd, LOWORD(wParam));
			PostQuitMessage(0);
			SendMessage(hWnd, WM_QUERYENDSESSION, NULL, NULL);
			return (INT_PTR)TRUE;
		}
		break;
	case WM_TRAYICON:
		if (lParam == WM_LBUTTONDOWN)
			ShowWindow(hWnd, SW_SHOW);
		break;
	case WM_CLOSE:
		doRemoveHook(hWnd);
		EndDialog(hWnd, LOWORD(wParam));
		PostQuitMessage(0);
		return (INT_PTR)TRUE;
	default:
		return FALSE;
	}

	return (INT_PTR)TRUE;
}

//Dialog warming proc
INT_PTR CALLBACK WarmingProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	WCHAR buf[120];
	int len;
	HFONT hFont;

	UNREFERENCED_PARAMETER(lParam);

	switch (message)
	{
	case WM_INITDIALOG:
		hFont = CreateFont(0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, TEXT("Courier New"));
		SendDlgItemMessage(hDlg, IDC_EDIT2, WM_SETFONT, (WPARAM)hFont, 0);
		SendDlgItemMessage(hDlg, IDC_TEXT, WM_SETFONT, (WPARAM)hFont, 0);

		SetDlgItemText(hDlg, IDC_TEXT, L"Vui long nhap mat khau de huy!");

		return (INT_PTR)TRUE;
	case WM_COMMAND:
		switch (wParam)
		{
		case IDOK:
			len = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT2));
			if (len > 0)
			{
				GetDlgItemText(hDlg, IDC_EDIT2, buf, len + 1);
				buf[len] = '\0';
			}

			if (lstrcmp(buf, L"123456789") == 0)
			{
				SetDlgItemText(hDlg, IDC_TEXT, L"Mo thanh cong");
				check = true;
				EndDialog(hDlg, LOWORD(wParam));

			}
			else 
				if (lstrcmp(buf, L"123456789") != 0)
				{
					SetDlgItemText(hDlg, IDC_TEXT, L"Mat khau cua ban nhap chua dung! \n Vui long nhap lai");
					check = false;
				}
			break;
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			break;
		default:
			return (INT_PTR)FALSE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void UpdateListBox(HWND hWnd, int ID)
{
	typedef VOID(*MYPROC)(HWND, int);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"Block Web.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_UpdateListBox");
		if (ProcAddr != NULL)
			ProcAddr(hWnd, ID);
	}
}

void UpdateWebFile(HWND hWnd, int ID)
{
	typedef VOID(*MYPROC)(HWND, int);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"Block Web.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_UpdateWebFile");
		if (ProcAddr != NULL)
			ProcAddr(hWnd, ID);
	}
}

void doInstallHook(HWND hWnd)
{
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"Block Web.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doInstallHook");
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}

void doRemoveHook(HWND hWnd)
{
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"Block Web.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doRemoveHook");

		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}

bool checkFileHost()
{
	typedef bool(*MYPROC)();

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"Block Web.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_checkFileHost");

		if (ProcAddr != NULL)
			return ProcAddr();
	}
}

void showInSystemTray(HWND hWnd)
{
	NOTIFYICONDATA nid = {};
	nid.cbSize = sizeof(nid);
	nid.hWnd = hWnd;
	nid.uFlags = NIF_ICON | NIF_TIP | NIF_GUID | NIF_MESSAGE;
	nid.uCallbackMessage = WM_TRAYICON;
	nid.uID = ID_TRAY_APP_ICON;

	// This text will be shown as the icon's tooltip.
	StringCchCopy(nid.szTip, ARRAYSIZE(nid.szTip), L"Lock Keyboard");

	// Load the icon for high DPI.
	nid.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_SMALL));

	// Show the notification.
	Shell_NotifyIcon(NIM_ADD, &nid) ? S_OK : E_FAIL;
}